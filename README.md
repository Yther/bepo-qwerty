# Qwerty et Bépo
*Une histoire git*

Bienvenu dans cette petite histoire qui sert d'initiation à git. Les histoires git sont des
histoires interactives qui se servent de git pour dérouler un récit à la manière d'un *"livre
dont vous êtes le héros"*.

La narration du texte est toujours dans le fichier histoire.md

Pour commencer, choisissez si vous préférez un **chien** ou un **chat**, et déplacez vous sur
le tag associé. (commande `git switch -d <chat|chien>`)
